//
//  Vue.m
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import "Vue.h"

@implementation Vue

-(void)viewDidLoad
{
	
	GLKView *vue=[[GLKView alloc]initWithFrame:self.frame context:[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES2]];
	[self addSubview:vue];
	[EAGLContext setCurrentContext:[vue context]];
	[vue setDelegate:self];
	GLKViewController * Controller=[[GLKViewController alloc] initWithNibName:nil bundle:nil];
	[Controller setView:(UIView *)vue];
	[Controller setDelegate:self];
	[Controller setPreferredFramesPerSecond:60];
	
}
-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
	
	glClearColorx(0, 256, 170, 256);
	glClear(GL_COLOR_BUFFER_BIT);
	 
}
-(void)glkViewControllerUpdate:(GLKViewController *)controller
{
	
}
-(void)glkViewController:(GLKViewController *)controller willPause:(BOOL)pause
{
	
}
@end
