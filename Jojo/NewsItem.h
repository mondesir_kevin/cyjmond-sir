//
//  NewsItem.h
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsItem : NSObject
@property(copy,readwrite) NSString *commentaire;
@property(copy,readwrite) NSString *datePubli;
@property(copy,readwrite) UIImage * _image;
@end
