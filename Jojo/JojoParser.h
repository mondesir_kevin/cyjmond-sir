//
//  JojoParser.h
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsItem.h"
@interface JojoParser : NSObject
{
	NSString * chaine;
	NSMutableArray * tableau;
}

@property (assign,readwrite)NSMutableArray * tableau;
@property (assign,readwrite)BOOL zoneNews;
-(id)initWithPath:(NSString *)path;
-(void)search;
@end
