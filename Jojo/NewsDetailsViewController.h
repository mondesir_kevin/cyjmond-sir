//
//  NewsDetailsViewController.h
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailsViewController : UIViewController <UITabBarDelegate>
{
	IBOutlet UINavigationItem * navBar;
	IBOutlet UITextView * textView;
	IBOutlet UIImageView * imageView;
}
@property (retain,readwrite)NSString * titre;
@property (retain,readwrite)NSString * commentaire;
@property (retain,readwrite)UIImage * imago;
-(void)setTitle:(NSString *)title;
-(void)setText:(NSString *)text;
-(void)setImage:(UIImage *)image;
@end
