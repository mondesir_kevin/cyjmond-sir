//
//  NewsTabController.h
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JojoParser.h"
#import "NewsDetailsViewController.h"
@interface NewsTabController : UITableViewController <NSXMLParserDelegate>
{
	JojoParser * parser;
	int state;
	NewsDetailsViewController * controller;
}
@property (readonly)JojoParser * parser;
@property (readonly)int state;
@property (assign,readwrite)BOOL ZoneNews;
-(void)tapped:(id)sender;
@end
