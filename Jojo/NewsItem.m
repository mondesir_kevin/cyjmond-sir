//
//  NewsItem.m
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import "NewsItem.h"

@implementation NewsItem
@synthesize commentaire;
@synthesize datePubli;
@synthesize _image;
-(void)dealloc
{
	[commentaire release];
	[datePubli release];
	[_image release];
	[super dealloc];
}
@end
