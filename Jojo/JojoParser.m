//
//  JojoParser.m
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//
// Parseur html
//Pour les news du blog http://johnnyhallyday.com 
#import "JojoParser.h"

@implementation JojoParser
@synthesize zoneNews;
@synthesize tableau;
-(id)init
{
	if([super init]){
		
	}
	return self;
}
-(id)initWithPath:(NSString *)path
{
	if([self init]){
		chaine=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
		tableau=[[NSMutableArray alloc]initWithCapacity:10];
		
	}
	return self;
}

-(void)search
{
	NSRange r1,r2;
	NSRange range;
	NewsItem * item;
	range=[chaine rangeOfString:@"<article"];
	while (![chaine isEqualToString:@""]&&(range.location!=NSNotFound||range.location!=NSNotFound)) {
		
		if(range.location!=NSNotFound||range.length!=NSNotFound){
			chaine=[chaine substringWithRange:NSMakeRange(range.location, chaine.length-range.location)];
			item=[[NewsItem alloc]init];
			r1=[chaine rangeOfString:@"<img src="];
			r2=[chaine rangeOfString:@" class=\"thumb\""];
			[item set_image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[chaine substringWithRange:NSMakeRange(r1.location+r1.length+1, r2.location-r1.location-r1.length-2)]]]]];
			
			chaine=[chaine substringWithRange:NSMakeRange(r2.location, chaine.length-r2.location)];
			r1=[chaine rangeOfString:@"<date datetime=\""];
			r2=[chaine rangeOfString:@"</date>"];
			[item setDatePubli:[chaine substringWithRange:NSMakeRange(r1.location+r1.length+27,12 )]];
			
			
			chaine=[chaine substringWithRange:NSMakeRange(r2.location, chaine.length-r2.location)];
			r1=[chaine rangeOfString:@"<div class=\"entry\">"];
			r2=[chaine rangeOfString:@"</div>"];
			[item setCommentaire:[chaine substringWithRange:NSMakeRange(r1.location+r1.length, r2.location-r1.location-r1.length)]];
			chaine=[chaine substringWithRange:NSMakeRange(r2.location, chaine.length-r2.location)];
			
			
			
			
			[tableau addObject:item];
			range=[chaine rangeOfString:@"<article"];
		}
		else{
			chaine=@"";
		}
		
	}
	
	
}
-(void)dealloc
{
	[chaine release];
	[tableau release];
	[super dealloc];
}


@end
