//
//  AppDelegate.h
//  Jojo
//
//  Created by Kévin Mondésir on 22/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
