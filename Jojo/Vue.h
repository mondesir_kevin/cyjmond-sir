//
//  Vue.h
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
@interface Vue : UIView <UITabBarDelegate,GLKViewControllerDelegate,GLKViewDelegate>

@end
