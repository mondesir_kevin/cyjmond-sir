//
//  NewsDetailsViewController.m
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import "NewsDetailsViewController.h"

@interface NewsDetailsViewController ()

@end

@implementation NewsDetailsViewController
@synthesize imago;
@synthesize titre;
@synthesize commentaire;
-(void)viewDidLoad
{
	[super viewDidLoad];
	[self setTitle:titre];
	[self setImage:imago];
	[self setText:commentaire];
}
-(void)setTitle:(NSString *)title
{
	[navBar setTitle:title];
}
-(void)setText:(NSString *)text
{
	[textView setEditable:YES];
	[textView setText:text];
	[textView setTextAlignment:NSTextAlignmentCenter];
	textView.alpha=0.8;
	textView.textColor=[UIColor colorWithRed:1 green:0.0 blue:0.6 alpha:1];
	[textView setEditable:FALSE];
	
}
-(void)setImage:(UIImage *)image
{
	[imageView setImage:image];
}
-(void)dealloc
{
	[titre release];
	[imago release];
	[commentaire release];
	[imageView release];
	[navBar release];
	[textView release];
	[super dealloc];
}



@end
