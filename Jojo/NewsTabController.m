//
//  NewsTabController.m
//  Jojo
//
//  Created by Kévin Mondésir on 23/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import "NewsTabController.h"

@implementation NewsTabController
@synthesize ZoneNews;
@synthesize state;
@synthesize parser;
-(void)viewDidLoad
{
	[super viewDidLoad];
	
	[self setZoneNews:FALSE];
	/*NSLog(@"%@",[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://johnnyhallyday.com"]]);*/
	
	NSString * path=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
	parser=[[JojoParser alloc]initWithPath:[path stringByAppendingString:@"/jojoWebSite.xml"] ];
	[parser search];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [[parser tableau]count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell * cell= [tableView
							 dequeueReusableCellWithIdentifier:@"News"];
	
	cell.textLabel.text = [[[parser tableau] objectAtIndex:[indexPath indexAtPosition:1]] datePubli];
	
	[cell.imageView setImage:[[[parser tableau] objectAtIndex:[indexPath indexAtPosition:1]] _image]];
	UIButton * button=[[UIButton alloc]initWithFrame:CGRectMake(160, 0, 60, 60)];
	[button setImage:[UIImage imageNamed:@"cygogne.png"] forState:UIControlStateNormal];
	[cell setAccessoryView:button];
	[button addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventTouchUpInside];
	[button setTitle:[NSString stringWithFormat:@"%d",[indexPath indexAtPosition:1]] forState:UIControlStateNormal];
	
	
	return cell;
}
-(void)tapped:(id)sender{
	state=[[[(UIButton *)sender titleLabel]text]intValue];
	[self performSegueWithIdentifier:@"motto" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([[segue identifier] isEqualToString:@"motto"]){
		controller=[segue destinationViewController];
		[controller setCommentaire:[[[parser tableau] objectAtIndex:state] commentaire] ] ;
		[controller setImago:[[[parser tableau] objectAtIndex:state] _image] ] ;
		[controller setTitre:[[[parser tableau] objectAtIndex:state] datePubli] ] ;
		
		
	}
}
-(void)dealloc
{
	[parser release];
	[NewsDetailsViewController release];
	[super dealloc];
}




@end
