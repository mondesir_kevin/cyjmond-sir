//
//  ViewController.h
//  Jojo
//
//  Created by Kévin Mondésir on 22/03/13.
//  Copyright (c) 2013 Kévin Mondésir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "etc.h"
#import "JojoParser.h"

@interface ViewController : UIViewController <NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITabBarDelegate>
{
	FILE * file;
}

@end
